import socket

HOST = '127.0.0.1'
PORT = 12345

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.connect((HOST, PORT))
    while True:
        data_in = input("give 2 coma separated numbers: ").encode("utf-8")
        if not data_in:
            break
        sock.sendall(data_in)
        data_out = sock.recv(1024)
        print(f"received data: {data_out.decode('utf-8')}")
