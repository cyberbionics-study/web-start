import socket
import time

HOST = '127.0.0.1'
PORT = 12345

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while True:
    device_id = "test"
    client_socket.sendto(device_id.encode('utf-8'), (HOST, PORT))
    print(f"Presence message sent: {device_id}")
    time.sleep(5)
