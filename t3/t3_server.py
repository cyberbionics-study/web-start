import socket

HOST = '127.0.0.1'
PORT = 12345


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.bind((HOST, PORT))
    sock.listen()
    conn, addr = sock.accept()
    with conn:
        print(f"connection by {addr}")
        while True:
            data = conn.recv(1024)
            if not data:
                break
            print(data.decode("utf-8"))
            output = data.decode("utf-8").split(',')
            print(output)
            res = 0
            for i in output:
                res += int(i)
            print(res)
            conn.sendall(str(res).encode("utf-8"))
