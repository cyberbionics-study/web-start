import requests


def do_request(url: str, method: str, data: dict = None):
    """
    Make an HTTP request.

    :param url: The URL for the request as a string.
    :param method: The HTTP method, either 'GET' or 'POST'.
    :param data: Optional data to be included in the request, provided as a dictionary.
    """
    method = method.upper()

    if method == 'GET':
        response = requests.get(url, params=data)
    elif method == 'POST':
        response = requests.post(url, json=data)
    else:
        print(f"Unsupported HTTP method: {method}")
        return None

    print(f"Status Code: {response.status_code}")
    print("Headers:")
    for header, value in response.headers.items():
        print(f"  {header}: {value}")

    print("Response Body:")
    print(response.text)


if __name__ == '__main__':
    do_request('https://jsonplaceholder.typicode.com/users', 'post')
