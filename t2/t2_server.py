import socket

HOST = '127.0.0.1'
PORT = 12345

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind((HOST, PORT))

print(f"The server is running on {HOST}:{PORT}. Waiting for messages...")

while True:
    data, address = server_socket.recvfrom(1024)
    device_id = data.decode('utf-8')
    print(f"New device detected: {device_id} from {address}")
