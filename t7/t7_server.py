import socket
import threading

HOST = '127.0.0.1'
PORT = 12345

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind((HOST, PORT))
server_socket.listen(5)

clients = []


def handle_client(client_socket):
    while True:
        message = client_socket.recv(1024).decode("utf-8")
        if not message:
            break
        print(f"Client: {message}")

        for client in clients:
            if client != client_socket:
                try:
                    client.send(message.encode("utf-8"))
                except:
                    clients.remove(client)

    client_socket.close()


while True:
    client_socket, client_address = server_socket.accept()
    print(f"Connected with {client_address}")

    clients.append(client_socket)

    client_thread = threading.Thread(target=handle_client, args=(client_socket,))
    client_thread.start()
