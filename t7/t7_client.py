import socket

HOST = '127.0.0.1'
PORT = 12345


def do_work(host, port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
        client_socket.connect((host, port))
        while True:
            message = input("Client: ")
            client_socket.send(message.encode("utf-8"))
            response = client_socket.recv(1024).decode("utf-8")
            print(f"Server: {response}")


if __name__ == '__main__':
    do_work(HOST, PORT)
