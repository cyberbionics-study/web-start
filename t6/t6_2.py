import json
from urllib import request
import requests as rq
from t6 import URL, data


url = URL + 'users'

data_1 = json.dumps(data).encode('utf-8')
req_1 = request.Request(url, data_1, headers={'Content-Type': 'application/json'})
response_1 = request.urlopen(req_1)
new_user_1 = json.loads(response_1.read().decode())
print(new_user_1)
response_2 = rq.post(url, json=data)
new_user_2 = response_2.json()
print(new_user_2)
