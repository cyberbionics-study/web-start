import json
from urllib import request
import requests as rq
from t6 import URL


url = URL + 'users'
responce_1 = request.urlopen(url)
data_1 = json.loads(responce_1.read().decode())
responce_2 = rq.get(url)
data_2 = responce_2.json()
print(f'output of using urllib: {data_1}')
print(f'output of using requests: {data_2}')
