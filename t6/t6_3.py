import json
from urllib import request
import requests as rq
from t6 import URL


user_id = 1
url = URL + f'users/{user_id}'
response_1 = request.urlopen(url)
user_data_1 = json.loads(response_1.read().decode())
print(user_data_1)
response_2 = rq.get(url)
user_data_2 = response_2.json()
print(user_data_2)
